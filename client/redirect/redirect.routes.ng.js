'use strict'

angular.module('urlShortenerApp')
.config(function($stateProvider) {
  $stateProvider
  .state('redirect', {
    templateUrl: 'client/redirect/redirect.view.html',
    controller: 'RedirectCtrl'
  });
});