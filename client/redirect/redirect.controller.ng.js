'use strict'

angular.module('urlShortenerApp')
.controller('RedirectCtrl', function($scope, $window, $location, $api, $state) {
    Meteor.call("getKeyword", function(err, res) {
        $api.login(res).then(function(d){
            $api.get("url/" + $location.path().substring(1)).then(function(d){
                if(d != null && d != undefined && d.url != null && d.url != undefined){
                    window.location.href = d.url;
                }
            },
            function(d) {
                if(d == '404'){
                    $state.go('main');
                }
            }); 
        });
    });
});