'use strict';

angular.module('urlShortenerApp')
.controller('MainController', function($scope, $api) {
    console.log(Meteor.subscribe('keywords'));
    
    $scope.save = function() {
        if ($scope.form.$valid) {
            Meteor.call("getKeyword", function(err, res) {
                $api.login(res).then(function(d){
                    $api.post("url/save", $scope.newLink).then(function(d){
                        if(d != undefined && d != null && d.msg == 'Save successfully!'){
                            alert("URL saved. Please access through http://localhost:3000/" + $scope.newLink.name);
                        }

                        $scope.newLink = '';
                    }, function(d){
                        if(d == "501"){
                            alert("Duplicated Name, please use another name.");
                        }

                        $scope.newLink = '';
                    });
                });
            });
        }
    }
});