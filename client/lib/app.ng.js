angular.module('urlShortenerApp', [
  'angular-meteor',
  'ui.router',
  'ngMaterial',
  'api.service'
]);

onReady = function() {
  angular.bootstrap(document, ['urlShortenerApp']);
};

/*if (Meteor.isClient) {
    var keywordsCollection = new Mongo.Collection("keywordsCollection");
    Meteor.subscribe("keywords");
    
    Template.keywords.helpers({
        keyword: function() {
            return keywordsCollection.findOne().keyword;
        }
    });
}*/


  
if(Meteor.isCordova) {
  angular.element(document).on('deviceready', onReady);
} else {
  angular.element(document).ready(onReady);
}