var apiUrl = 'http://localhost:8080/api/';

angular.module('api.service',[])
.factory('$api',function($rootScope,$http,$q,$window,$state){
  var api = {};  
  api.authenticated = false;
  api.request = function(method,url,data){
    var df = $q.defer();     
    var config = {
      method: method,
      url: apiUrl + url
    }
    if(typeof(data)!='undefined')
      config.data = data;
    $http(config).then(function(d){
      if(d.status == 200)
        df.resolve(d.data)
      else
        df.reject(d.status);
    },function(d){
      if(d.status == 401){
        console.log("Unauthorize");
        df.reject(d.status);
      }
      else{
        df.reject(d.status);
      }
    })
    return df.promise;
  }
  api.get = function(url){
    return this.request('GET',url);
  }
  api.post = function(url,data){
    return this.request('POST',url,data);
  }
  api.put = function(url,data){
    return this.request('PUT',url,data);
  }
  api.delete = function(url){
    return this.request('DELETE',url);
  }
  api.login = function(keyword){
    var df = $q.defer();
    api.authenticated = false;
    delete $http.defaults.headers.common.Authorization;
    this.post('token',{keyword:keyword}).then(function(d){
      if(angular.isDefined(d.token)){
        $http.defaults.headers.common.Authorization = 'Bearer '+ d.token;
        $window.localStorage['token'] = d.token;
        api.authenticated = true;
        df.resolve();
      }
      else
        df.reject();
    },function(){df.reject('Network Failed')});
    return df.promise;
  }
  api.logout = function(){
    delete $http.defaults.headers.common['Authorization'];
    delete $window.localStorage['token'];
  }
  return api;
})