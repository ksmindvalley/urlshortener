'use strict';

angular.module('urlShortenerApp')

.config(function($urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true);  
  $urlRouterProvider.otherwise(function($injector, $location){
      $injector.invoke(['$state', function($state) {
          $state.go('redirect');
      }]);
  });
});